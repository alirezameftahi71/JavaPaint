

# Java Paint
A simple paint project using java swing. <br />
Note that this program uses MySQL database to store each user's paintings and the user authentication information.<br />So you have to create the database before you run the program.<br />
Follow these steps:

 1. Copy and paste the SQL script below, in a text editor and save it by the `.sql` format.
 2. Open a mysql shell and import the sql script in the your MySQL RDBM.
 3. Open the project in an IDE. (I used Eclipse to create the project)
 4. Make sure the JDBC connector is added to the project. If not, add it manually.
 5. Under the package `database` there's a java class called `DbConnector`. Open it and in line number 21 change the value for the password variable as it fits your MySQL configuration.
 6. Compile and run the program.

Here is the SQL script:

<pre>
/*
 * Alireza Meftahi
 * Java Paint
 * MySQL Script
 */

 -- Database Creation
DROP DATABASE IF EXISTS java_paint_db;
CREATE DATABASE java_paint_db;
USE java_paint_db;


-- Tables Craetion
CREATE TABLE users (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  username varchar(20),
  password varchar(20)
);

CREATE TABLE shapes (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_id int(11) REFERENCES users(id),
  type varchar(20),
  x1 int(11),
  y1 int(11),
  x2 int(11),
  y2 int(11),
  filled bool,
  color varchar(20)
);


-- Insertion into the Tables
INSERT INTO users (username, password) VALUES 
	('root', 'admin');
</pre>







