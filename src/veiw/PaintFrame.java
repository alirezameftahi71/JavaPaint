package veiw;

import java.awt.EventQueue;
import javax.swing.JFrame;
import model.*;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JToggleButton;
import javax.swing.ButtonGroup;
import java.awt.Color;
import control.ShapeManager.ShapeList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JCheckBox;

public class PaintFrame {

	// fields
	private JFrame paintFrame;
	PanelPaint _canvas = new PanelPaint();
	JPanel container = new JPanel();
	private final ButtonGroup buttonGroup = new ButtonGroup();

	// constructors
	/**
	 * Create the application.
	 */
	public void showFrame() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					PaintFrame window = new PaintFrame();
					window.paintFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PaintFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		paintFrame = new JFrame();
		paintFrame.setTitle("JPaint");
		paintFrame.setResizable(false);
		paintFrame.setBounds(100, 100, 650, 500);
		paintFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		paintFrame.setContentPane(container);
		container.setLayout(null);
		container.add(_canvas);
		// _canvas.setLayout(null);

		JPanel panelToolbox = new JPanel();
		panelToolbox.setBounds(500, 0, 150, 500);
		container.add(panelToolbox);
		panelToolbox.setLayout(null);

		JLabel lblShapes = new JLabel("Shapes");
		lblShapes.setBounds(32, 11, 89, 14);
		panelToolbox.add(lblShapes);

		JButton btnLine = new JButton("Line");
		btnLine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_canvas.setShape(ShapeList.LINE);
			}
		});
		btnLine.setBounds(27, 36, 94, 23);
		panelToolbox.add(btnLine);

		JButton btnCircle = new JButton("Circle");
		btnCircle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_canvas.setShape(ShapeList.OVAL);
			}
		});
		btnCircle.setBounds(27, 62, 94, 23);
		panelToolbox.add(btnCircle);

		JButton btnRectangle = new JButton("Rectangle");
		btnRectangle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_canvas.setShape(ShapeList.RECTANGLE);
			}
		});
		btnRectangle.setBounds(27, 87, 94, 23);
		panelToolbox.add(btnRectangle);

		JLabel lblColors = new JLabel("Colors");
		lblColors.setBounds(27, 127, 94, 14);
		panelToolbox.add(lblColors);

		JRadioButton rdbtnBlack = new JRadioButton("Black");
		rdbtnBlack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_canvas.setColor(Color.BLACK);
				_canvas.changeColor(Color.BLACK);
			}
		});
		buttonGroup.add(rdbtnBlack);
		rdbtnBlack.setSelected(true);
		rdbtnBlack.setBounds(32, 148, 89, 23);
		panelToolbox.add(rdbtnBlack);

		JRadioButton rdbtnRed = new JRadioButton("Red");
		rdbtnRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_canvas.setColor(Color.RED);
				_canvas.changeColor(Color.RED);
			}
		});
		rdbtnRed.setForeground(Color.RED);
		buttonGroup.add(rdbtnRed);
		rdbtnRed.setBounds(32, 174, 89, 23);
		panelToolbox.add(rdbtnRed);

		JRadioButton rdbtnBlue = new JRadioButton("Blue");
		rdbtnBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_canvas.setColor(Color.BLUE);
				_canvas.changeColor(Color.BLUE);
			}
		});
		rdbtnBlue.setForeground(Color.BLUE);
		buttonGroup.add(rdbtnBlue);
		rdbtnBlue.setBounds(32, 200, 89, 23);
		panelToolbox.add(rdbtnBlue);

		JRadioButton rdbtnGreen = new JRadioButton("Green");
		rdbtnGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_canvas.setColor(Color.GREEN);
				_canvas.changeColor(Color.GREEN);
			}
		});
		rdbtnGreen.setForeground(Color.GREEN);
		buttonGroup.add(rdbtnGreen);
		rdbtnGreen.setBounds(32, 226, 89, 23);
		panelToolbox.add(rdbtnGreen);

		JRadioButton rdbtnYellow = new JRadioButton("Yellow");
		rdbtnYellow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_canvas.setColor(Color.YELLOW);
				_canvas.changeColor(Color.YELLOW);
			}
		});
		rdbtnYellow.setForeground(Color.YELLOW);
		buttonGroup.add(rdbtnYellow);
		rdbtnYellow.setBounds(32, 253, 89, 23);
		panelToolbox.add(rdbtnYellow);

		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				LoginFrame loginFrame = new LoginFrame();
				loginFrame.main(null);
				paintFrame.setVisible(false);
			}
		});
		btnLogout.setBounds(27, 430, 94, 23);
		panelToolbox.add(btnLogout);

		JButton btnZoomIn = new JButton("+");
		btnZoomIn.setFont(new Font("Dialog", Font.BOLD, 14));
		btnZoomIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_canvas.zoomIn();
			}
		});

		JButton btnZoomOut = new JButton("-");
		btnZoomOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_canvas.zoomOut();
			}
		});
		btnZoomOut.setFont(new Font("Dialog", Font.BOLD, 14));
		btnZoomOut.setBounds(25, 396, 45, 24);
		panelToolbox.add(btnZoomOut);
		btnZoomIn.setBounds(80, 396, 45, 24);
		panelToolbox.add(btnZoomIn);

		JCheckBox chckbxFillShape = new JCheckBox("Fill Shape");
		chckbxFillShape.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_canvas.toggleFilled();
			}
		});
		chckbxFillShape.setBounds(32, 286, 89, 23);
		panelToolbox.add(chckbxFillShape);

		JToggleButton btnSelectActive = new JToggleButton("Select Off");
		btnSelectActive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean active = _canvas.toggleSelectActivated();
				if (active) {
					btnSelectActive.setText("Select On");
				} else {
					btnSelectActive.setText("Select Off");
				}
			}
		});
		btnSelectActive.setBounds(27, 325, 94, 23);
		panelToolbox.add(btnSelectActive);

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_canvas.deleteShape();
			}
		});
		btnDelete.setBounds(27, 359, 94, 23);
		panelToolbox.add(btnDelete);
	}
}
