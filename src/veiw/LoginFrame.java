package veiw;

import java.awt.Color;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import control.LogManager;
import control.ShapeManager;
import database.*;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.event.ActionEvent;

/**
 * This Class is the Login Form which user can interact with and pass in user and
 * password to login or sign up
 *
 * @author alireza
 */
public class LoginFrame {

	private JFrame loginFrame;
	private JTextField tfUsername;
	private JPasswordField tfPassword;
	private JLabel lbLogin;
	private PaintFrame paintFrame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginFrame window = new LoginFrame();
					window.loginFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		loginFrame = new JFrame();
		loginFrame.setTitle("Login");
		loginFrame.setResizable(false);
		loginFrame.setBounds(100, 100, 391, 240);
		loginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		loginFrame.getContentPane().setLayout(null);

		tfUsername = new JTextField();
		tfUsername.setBounds(33, 91, 166, 28);
		loginFrame.getContentPane().add(tfUsername);
		tfUsername.setColumns(10);

		JLabel lbLogin = new JLabel(
				"<html>\r\nPlease Login.<br />\r\nOr if you don't have an account, register.\r\n</html>");
		lbLogin.setBounds(23, 11, 301, 80);
		loginFrame.getContentPane().add(lbLogin);

		tfPassword = new JPasswordField();
		tfPassword.setBounds(33, 140, 166, 27);
		loginFrame.getContentPane().add(tfPassword);

		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// if the text fileds are not empty or filled with just white spaces
				if (!(tfUsername.getText().trim().equals("")) && !(tfPassword.getText().trim().equals(""))) {
					// get the input username and password from the text fields
					String username = tfUsername.getText().trim();
					String password = tfPassword.getText().trim();
					// check if the user and pass are valid
					if (LogManager.isValidUser(username, password)) {
						// create a new instance of the paint window and hide this form
						LogManager.userId = LogManager.getId(username);
						paintFrame = new PaintFrame();
						paintFrame.showFrame();
						loginFrame.setVisible(false);
					} else {
						// show an error message
						lbLogin.setText("Username or password is wrong!");
						lbLogin.setForeground(Color.red);
					}
				} else {
					lbLogin.setText("Username or password cannot be blank!");
					lbLogin.setForeground(Color.red);
				}
			}
		});
		btnLogin.setBounds(244, 140, 106, 27);
		loginFrame.getContentPane().add(btnLogin);

		JButton btnSignUp = new JButton("Sign Up");
		btnSignUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// if the text fileds are not empty or filled with just white spaces
				if (!(tfUsername.getText().trim().equals("")) && !(tfPassword.getText().trim().equals(""))) {
					// get the input username and password from the text fields
					String username = tfUsername.getText().trim();
					String password = tfPassword.getText().trim();
					// check if the user is already existed
					// creating the query
					String query = "SELECT count(id) as 'count' " + "FROM users WHERE username='" + username + "';";
					// boolean variable with initial value of false
					boolean alreadyExisted = false;
					try {
						// connection to the database
						DbConnector.startConnection();
						// storing the results to a resutlSet obj
						ResultSet resultSet = DbConnector.getStatement().executeQuery(query);
						resultSet.next();
						// if there was a row or more back
						if (resultSet.getInt("count") > 0) {
							// set the bool var true
							alreadyExisted = true;
						}
					} catch (SQLException e) {
						e.printStackTrace();
					} finally {
						// close the connection
						DbConnector.closeConnection();
					}
					// if the user was not already existed, insert them as new user
					if (!alreadyExisted) {
						// create insert query
						query = "INSERT INTO users(username, password) VALUES('" + username + "', '" + password + "')";
						try {
							// connection to the database
							DbConnector.startConnection();
							// execute query
							DbConnector.getStatement().executeUpdate(query);
							// set the message that the user was registered successfully
							lbLogin.setText("Registered Successfully. You can login now.");
							lbLogin.setForeground(Color.GREEN);
						} catch (SQLException ex) {
							Logger.getLogger(ShapeManager.class.getName()).log(Level.SEVERE, null, ex);
						} finally {
							// close the connection
							DbConnector.closeConnection();
						}
					} else {
						// if the user was already existed, set error message
						lbLogin.setText("Username already Existed.");
						lbLogin.setForeground(Color.RED);
					}
					// if the username or password were blank
				} else {
					lbLogin.setText("Username or password cannot be blank!");
					lbLogin.setForeground(Color.red);
				}
			}
		});
		btnSignUp.setBounds(244, 92, 106, 27);
		loginFrame.getContentPane().add(btnSignUp);

	}
}
