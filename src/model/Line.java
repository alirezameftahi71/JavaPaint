package model;

import java.awt.Graphics;
import control.*;

/**
 * This class is used to create Line objects
 *
 * @author alireza
 */
public class Line extends Shape {

	/**
	 * @param graphics
	 * @return graphics Loads a Line shape into the graphics object and returns it.
	 */
	@Override
	public void draw(Graphics graphics) {
		// setting the color of the graphics based on the color of the shape
		graphics.setColor(color);
		// drawing a Line into the graphics
		graphics.drawLine(start.getX(), start.getY(), end.getX(), end.getY());
	}
}
