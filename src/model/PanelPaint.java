package model;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;

import control.LogManager;
import control.Point;
import control.Shape;
import control.ShapeManager.ShapeList;
import control.ShapeManager;

public class PanelPaint extends JPanel implements MouseListener, MouseMotionListener {

	// fileds
	private ShapeList shapeType = ShapeList.LINE;
	private Color color = Color.BLACK;
	private ArrayList<Shape> shapes = new ArrayList<Shape>();
	private Shape shape = new Line();
	private double scaleX = 1;
	private double scaleY = 1;
	private boolean filled = false;
	private boolean selectionActivated = false;
	private Shape selectedShape = null;

	// constructor
	public PanelPaint() {
		setBackground(Color.white);
		setSize(500, 500);
		shape.setColor(color);
		shape.setStartPoint(new Point(0, 0));
		shape.setEndPoint(new Point(0, 0));
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		shapes = ShapeManager.getShapefromDb(LogManager.userId);
		this.repaint();
	}

	/**
	 * @return selectedShape
	 */
	public Shape getSelectedShape() {
		return this.selectedShape;
	}

	/**
	 * Zoom in the canvas
	 * 
	 */
	public void zoomIn() {
		this.scaleX = 2;
		this.scaleY = 2;
		this.repaint();
	}

	/**
	 * Zoom out the canvas
	 * 
	 */
	public void zoomOut() {
		this.scaleX = 1;
		this.scaleY = 1;
		this.repaint();
	}

	/**
	 * Toggle the filled flag
	 */
	public void toggleFilled() {
		this.filled = !this.filled;
	}

	/**
	 * @param shape
	 *            sets the type of the current shape from the shapelist enum
	 */
	public void setShape(ShapeList shape) {
		this.shapeType = shape;
	}

	/**
	 * @param color
	 *            sets the current shape's color
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		int w = this.getWidth(); // real width of canvas
		int h = this.getHeight(); // real height of canvas
		// Translate used to make sure scale is centered
		g2.translate(w / 2, h / 2);
		g2.scale(scaleX, scaleY);
		g2.translate(-w / 2, -h / 2);
		drawAll(g2); // draw all the shapes in the shapes list using the drawAll() method
		if (this.shape != null)
			shape.draw(g2); // draw the dragging shape
	}

	/**
	 * @param g
	 *            draws all the shapes in the shape list
	 */
	public void drawAll(Graphics2D g2) {
		for (Shape shape : shapes) {
			shape.draw(g2);
		}
	}

	/**
	 * @param shape
	 *            adds a new shape to both shapes list and the database using the
	 *            static addShape() method from ShapeManager class
	 */
	public void addCurShape(Shape shape) {
		shapes.add(shape);
		ShapeManager.addShape(shape, LogManager.userId);
	}

	/**
	 * toggles select mode
	 */
	public boolean toggleSelectActivated() {
		this.selectionActivated = !this.selectionActivated;
		return this.selectionActivated;
	}

	/**
	 * delete a shape both from panel and db.
	 */
	public void deleteShape() {
		if (this.selectedShape != null) { // if something is selected
			this.shapes.remove(selectedShape); // remove it from the list
			ShapeManager.deleteShapeFromDb(selectedShape); // remove it from the db
			this.repaint();
			this.selectedShape = null;
		} else {
			System.out.println("Nothing is selected.");
		}
	}

	/**
	 * @param color
	 *            changes the color of the selected shape based on the passed color
	 */
	public void changeColor(Color color) {
		if (this.selectedShape != null) {
			ShapeManager.updateShapeColor(selectedShape, color);
			this.selectedShape.setColor(color);
			this.repaint();
		}
	}

	// mousePressed
	@Override
	public void mousePressed(MouseEvent e) {
		if (!this.selectionActivated) {
			// create a new shape object based on user's choice from toolbox
			switch (shapeType) {
			case OVAL:
				shape = new Circle();
				break;
			case RECTANGLE:
				shape = new Rectangle();
				break;
			default:
				shape = new Line();
				break;
			}
			// set the color based on user's choice from the toolbox
			shape.setColor(color);
			// set if the shape is filled based on the filled checkbox on the frame
			shape.setFilled(filled);
			// save start point. this won't change for this shape
			shape.setStartPoint(new Point(e.getX(), e.getY()));
			// set end point as same as start for now. this will change as the mouse moves
			shape.setEndPoint(new Point(e.getX(), e.getY()));
		} else {
			this.selectedShape = null; // dispose of previous selected shape
			for (int i = 0; i < this.shapes.size(); i++) { // loop through shapes
				// check if the coordinates of the clicked point matches any shapes in the
				// shapes list. Note that this will only select the last match at the end.
				if ((e.getX() >= Math.min(this.shapes.get(i).getStartPoint().getX(),
						this.shapes.get(i).getEndPoint().getX())
						&& e.getX() <= Math.max(this.shapes.get(i).getStartPoint().getX(),
								this.shapes.get(i).getEndPoint().getX()))
						&& (e.getY() >= Math.min(this.shapes.get(i).getStartPoint().getY(),
								this.shapes.get(i).getEndPoint().getY())
								&& e.getY() <= Math.max(this.shapes.get(i).getStartPoint().getY(),
										this.shapes.get(i).getEndPoint().getY()))) {
					this.selectedShape = this.shapes.get(i); // set the last match to the selected shape
				}
			}
			System.out.println((this.selectedShape != null ? selectedShape.getClass().getSimpleName() : "null"));
		}
	}

	// mouseDragged
	@Override
	public void mouseDragged(MouseEvent e) {
		if (!this.selectionActivated) {
			shape.setEndPoint(new Point(e.getX(), e.getY())); // Set end point of drag. May change.
			this.repaint(); // After change, show new shape
		}
	}

	// mouseReleased
	@Override
	public void mouseReleased(MouseEvent e) {
		if (!this.selectionActivated) {
			try {
				shape.setEndPoint(new Point(e.getX(), e.getY())); // set the end point.
				addCurShape(shape); // add the shape to the shapes list and db
				this.shape = null;
				this.repaint(); // After change, show new shape
			} catch (NullPointerException ne) {
				System.out.println("Error in getting the coordinations of the mouse.");
			}
		}
	}

	// ignored mouse listeners
	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}
}