package model;

import java.awt.Graphics;
import control.*;

/**
 * This class is used to create Rectangle objects
 *
 * @author alireza
 */
public class Rectangle extends Shape {

	/**
	 * @param graphics
	 * @return graphics Loads an oval shape into the graphics object and returns it.
	 */
	@Override
	public void draw(Graphics graphics) {
		// the point object is inherited from the Shape class
		// calculating the length and the width
		int length = Math.abs(start.getX() - end.getX());
		int width = Math.abs(start.getY() - end.getY());
		// setting the color of the graphics based on the color of the shape
		graphics.setColor(color);
		// drawing a rect into the graphics
		if (this.filled) {
			graphics.fillRect(getMinX(), getMinY(), length, width);
		} else {
			graphics.drawRect(getMinX(), getMinY(), length, width);
		}

	}

	/**
	 * @return minimumX This method is used to draw the correct object even if the
	 *         user drags the mouse from bottom to up
	 */
	public int getMinX() {
		return Math.min(start.getX(), end.getX());
	}

	/**
	 * @return minimumY This method is used to draw the correct object even if the
	 *         user drags the mouse from bottom to up
	 */
	public int getMinY() {
		return Math.min(start.getY(), end.getY());
	}

}
