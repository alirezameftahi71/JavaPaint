/*
 * Alireza Meftahi
 * Java Paint
 * MySQL Script
 */

 -- Database Creation
DROP DATABASE IF EXISTS java_paint_db;
CREATE DATABASE java_paint_db;
USE java_paint_db;


-- Tables Craetion
CREATE TABLE users (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  username varchar(20),
  password varchar(20)
);

CREATE TABLE shapes (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_id int(11) REFERENCES users(id),
  type varchar(20),
  x1 int(11),
  y1 int(11),
  x2 int(11),
  y2 int(11),
  filled bool,
  color varchar(20)
);


-- Insertion into the Tables
INSERT INTO users (username, password) VALUES 
	('root', 'admin');
