package database;

import java.sql.*;

/**
 * This class manages the connection to the database. It contains methods for
 * connecting and disconnecting to the database. Note that the mysql variables
 * including the url and username and password, are pre set. Change them based
 * on your mysql configuration.
 *
 * @author alireza
 */
public class DbConnector {

	// JDBC driver name and database URL
	/*private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";*/
	private static final String DB_URL = "jdbc:mysql://localhost/java_paint_db?serverTimezone=UTC&useSSL=false";

	// database credentials
	private static final String USER = "root";
	private static final String PASS = "";
	private static Connection connection;
	private static Statement statement;

	/**
	 * @return connection variable
	 */
	public static java.sql.Connection getConnection() {
		return connection;
	}

	/**
	 * @return statement variable
	 */
	public static Statement getStatement() {
		return statement;
	}

	/**
	 * Starts connection to the database.
	 */
	public static void startConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			// System.out.println("Connecting to database...");
			connection = DriverManager.getConnection(DB_URL, USER, PASS);
			statement = connection.createStatement();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Closes connection to the database
	 */
	public static void closeConnection() {
		try {
			if (statement != null) {
				statement.close();
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}
}