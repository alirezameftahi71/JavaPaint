package control;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import database.*;

/**
 * Class LogManager deals with sign in and sign up functionalities.
 * 
 * @author alireza
 */
public class LogManager {

	/**
	 * Holds the id for the singed in user.
	 */
	public static int userId;

	/**
	 * @param username
	 * @param password
	 *            Adds a new user to the database.
	 */
	public static void addUser(String username, String password) {
		// creating the insert query
		String query = "INSERT INTO users(username, password) " + "VALUES('" + username + "','" + password + "');";
		try {
			// starting the connection
			DbConnector.startConnection();
			// executing the query
			DbConnector.getStatement().executeUpdate(query);
		} catch (SQLException ex) {
			Logger.getLogger(LogManager.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			// closing the connection
			DbConnector.closeConnection();
		}
	}

	/**
	 * @param username
	 * @return the id for the passed username.
	 */
	public static int getId(String username) {
		// creating the query
		String query = "SELECT id FROM users " + "WHERE username='" + username + "' LIMIT 1;";
		// definition and initial value of the variable 'id' of type integer
		int id = 0;
		try {
			// starting the connection
			DbConnector.startConnection();
			// executing the query and storing the result table in a ResutlSet
			ResultSet resultSet = DbConnector.getStatement().executeQuery(query);
			// storing the value of the column named 'id' in the variable 'id'
			resultSet.next();
			id = resultSet.getInt("id");
		} catch (SQLException ex) {
			Logger.getLogger(LogManager.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			DbConnector.closeConnection(); // close connection
			return id;
		}
	}

	/**
	 * @param username
	 * @return password for the username from the database.
	 */
	private static String getPassword(String username) {
		// creating the query
		String query = "SELECT password FROM users " + "WHERE username='" + username + "' LIMIT 1;";
		// definition and initial value for the variable 'password'
		String password = "";
		try {
			// starting the connection
			DbConnector.startConnection();
			// executing the query and storing the result in a ResultSet
			ResultSet resultSet = DbConnector.getStatement().executeQuery(query);
			// storing the value of column 'password' in the 'password' variable
			resultSet.next();
			password = resultSet.getString("password");
		} catch (SQLException ex) {
			Logger.getLogger(LogManager.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			DbConnector.closeConnection(); // closing the conneciton
			return password;
		}
	}

	/**
	 * @param username
	 * @param password
	 * @return true if username and password are valid. False if they're not.
	 */
	public static boolean isValidUser(String username, String password) {
		// if the password for the passed username matches the passed password, returns
		// true else false
		return getPassword(username).equals(password) ? true : false;
	}
}
