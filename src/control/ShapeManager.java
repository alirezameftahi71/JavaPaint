package control;

import java.awt.Color;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.glass.ui.TouchInputSupport;

import database.DbConnector;
import model.*;

/**
 * This class handles the shape functionalities
 *
 * @author alireza
 */
public class ShapeManager {

	// list of all the available shapes
	public static enum ShapeList {
		RECTANGLE, OVAL, LINE
	}

	/**
	 * @param type
	 * @param point
	 * @param userId
	 *            Adds the shape to the dataBase
	 */
	public static void addShape(Shape shape, int userId) {
		// creating the query string
		String query = "INSERT INTO shapes(user_id, type, x1, y1, x2, y2, filled, color) " + "VALUES(" + userId + ", '"
				+ shape.getClass().getSimpleName() + "', " + shape.start.getX() + "," + shape.start.getY() + ","
				+ shape.end.getX() + "," + shape.end.getY() + "," + shape.filled + ",'" + getColorName(shape.color)
				+ "');";
		try {
			DbConnector.startConnection(); // starting the connection to the db
			DbConnector.getStatement().executeUpdate(query); // execution of the query string
		} catch (SQLException ex) {
			Logger.getLogger(ShapeManager.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			DbConnector.closeConnection(); // closing the connection to the db
		}
	}

	/**
	 * @param color
	 * @return string of the name of the color that is passed as an arg
	 */
	private static String getColorName(Color color) {
		String colorname = "black";
		if (color.equals(Color.RED)) {
			colorname = "red";
		}
		if (color.equals(Color.GREEN)) {
			colorname = "green";
		}
		if (color.equals(Color.BLUE)) {
			colorname = "blue";
		}
		if (color.equals(Color.YELLOW)) {
			colorname = "yellow";
		}
		return colorname;
	}

	/**
	 * @param userId
	 * @return shapes list of all the shapes in the database that matching the
	 *         passed user id
	 */
	public static ArrayList<Shape> getShapefromDb(int userId) {
		// Initialing the shapes list to be returned
		ArrayList<Shape> shapes = new ArrayList<>();
		// creating the query string
		String query = "SELECT * FROM shapes WHERE user_id=" + userId;
		try {
			DbConnector.startConnection(); // start the connection to the db
			// storing the returned table to a ResultSet obj after executing the query
			ResultSet resultSet = DbConnector.getStatement().executeQuery(query);
			// as long as the table still has entry
			while (resultSet.next()) {
				// getting the data into separated variables
				String type = resultSet.getString("type");
				int x1 = resultSet.getInt("x1");
				int y1 = resultSet.getInt("y1");
				int x2 = resultSet.getInt("x2");
				int y2 = resultSet.getInt("y2");
				boolean filled = resultSet.getBoolean("filled");
				Point _start = new Point(x1, y1);
				Point _end = new Point(x2, y2);
				String color = resultSet.getString("color");
				// creating a shape based on the type returned from db, using the getShapeType()
				// method and setting it's properties using the data extracted from the db
				Shape shape = getShapeType(type);
				shape.setColor(getColor(color));
				shape.setStartPoint(_start);
				shape.setEndPoint(_end);
				shape.filled = filled;
				// adding the shape to the list
				shapes.add(shape);
			}

		} catch (SQLException ex) {
			Logger.getLogger(ShapeManager.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			DbConnector.closeConnection(); // closing the connection
		}
		return shapes; // returning the shapes list
	}

	/**
	 * @param type
	 * @return shape based on passed string
	 */
	private static Shape getShapeType(String type) {
		switch (type) {
		case ("Rectangle"):
			return new Rectangle();
		case ("Circle"):
			return new Circle();
		default:
			return new Line();
		}
	}

	/**
	 * @param color
	 * @return color based on the passed string
	 */
	private static Color getColor(String color) {
		switch (color) {
		case ("red"):
			return Color.RED;
		case ("green"):
			return Color.GREEN;
		case ("yellow"):
			return Color.YELLOW;
		case ("blue"):
			return Color.BLUE;
		default:
			return Color.BLACK;
		}
	}

	/**
	 * @param shape
	 * @return id of the passed shape in the db
	 */
	private static int getShapeId(Shape shape) {
		int id = 0;
		String query = "SELECT id FROM shapes WHERE user_id=" + LogManager.userId + " AND type='"
				+ shape.getClass().getSimpleName() + "' AND x1=" + shape.getStartPoint().getX() + " AND y1="
				+ shape.getStartPoint().getY() + " AND x2=" + shape.getEndPoint().getX() + " AND y2="
				+ shape.getEndPoint().getY() + " AND filled=" + (shape.filled ? 1 : 0) + " AND color='"
				+ getColorName(shape.color) + "';";
		DbConnector.startConnection();
		try {
			ResultSet resultSet = DbConnector.getStatement().executeQuery(query);
			resultSet.next();
			id = resultSet.getInt("id");
		} catch (SQLException e) {
			// e.printStackTrace();
			System.out.println("Nothing is selected!");
		} finally {
			DbConnector.closeConnection();
			return id;
		}
	}

	/**
	 * @param shape
	 *            deletes a shape from the db
	 */
	public static void deleteShapeFromDb(Shape shape) {
		int shapeId = getShapeId(shape);
		if (shapeId != 0) {
			String query = "DELETE FROM shapes WHERE id=" + shapeId + ";";
			DbConnector.startConnection();
			try {
				DbConnector.getStatement().executeUpdate(query);
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				DbConnector.closeConnection();
			}
		}
	}

	public static void updateShapeColor(Shape shape, Color color) {
		int shapeId = getShapeId(shape);
		if (shapeId != 0) {
			String query = "UPDATE shapes set color='" + getColorName(color) + "' WHERE id=" + shapeId + ";";
			DbConnector.startConnection();
			try {
				DbConnector.getStatement().executeUpdate(query);
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				DbConnector.closeConnection();
			}
		}
	}
}
