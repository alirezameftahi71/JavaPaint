package control;

import java.awt.Color;
import java.awt.Graphics;

public class Shape {
	/**
	 * This class is an abstract class and should be extended by other classes
	 *
	 * @author alireza
	 */
	protected Point start; // the starting point of shape
	protected Point end; // the finishing point of shape
	protected Color color; // the color of the shape
	protected boolean filled; // the shape is filled or not

	/**
	 * @param bool
	 * 
	 */
	public void setFilled(boolean filled) {
		this.filled = filled;
	}

	/**
	 * @param color
	 *            sets the color of the shape
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * @param start
	 *            sets the starting point of the shape
	 */
	public void setStartPoint(Point start) {
		this.start = start;
	}

	/**
	 * @return start
	 */
	public Point getStartPoint() {
		return this.start;
	}

	/**
	 * @param end
	 *            sets the ending point of the shape
	 */
	public void setEndPoint(Point end) {
		this.end = end;
	}

	/**
	 * @return end
	 * */
	public Point getEndPoint() {
		return this.end;
	}

	// draw method to be implemented by the derived classes.
	public void draw(Graphics g) {
	}
}
