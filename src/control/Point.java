package control;

/**
 * This Class handles the coordinates of the mouse
 * 
 * @author alireza
 */
public class Point {

	// the coordinations of the mouse
	private int x, y;

	/**
	 * @param x
	 * @param y
	 *            Constructor method creates a new Point object.
	 */
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	// empty constructor for later use
	public Point() {
	}

	// getters and setters
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

}
